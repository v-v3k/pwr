package com.example.comparator;

import java.util.Comparator;

public class BrandName implements Comparator<Cars>{
	@Override
	public int compare(Cars obj1, Cars obj2)
	{
		Cars car1=obj1;
		Cars car2=obj2;
		return car1.manufacturerName.compareTo(car2.manufacturerName);
	}
}