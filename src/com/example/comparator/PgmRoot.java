package com.example.comparator;

import java.util.ArrayList;
import java.util.Collections;

public class PgmRoot {
	public static void main(String[] args)
	{
		ArrayList<Cars> al=new ArrayList<Cars>();
		
		// Add data to class
		al.add(new Cars("Nissan","GTR",150000));
		al.add(new Cars("BMW","M3 Coupe",240000));
		al.add(new Cars("Aston Martin","DB9",185000));
		al.add(new Cars("Bugati","Veyron Super Sport",665000));
		
		// Sort by mode name
		Collections.sort(al, new ModelName());
		System.out.println("Sorted by model name: ");
		for(Cars i:al)
			System.out.println("Manufacturer: "+i.manufacturerName+
					" Model: "+i.carModel+"Price :"+
					i.carPrice);
		System.out.println();
		
		// Sort by manufacturer name
		Collections.sort(al, new BrandName());
		System.out.println("Sorted by manufacturer name: ");
		for(Cars i:al)
			System.out.println("Manufacturer: "+i.manufacturerName+
					" Model: "+i.carModel+"Price :"+
					i.carPrice);
		System.out.println();
		
		// Sort by price
		Collections.sort(al, new BrandName());
		System.out.println("Sorted by car price: ");
		for(Cars i:al)
			System.out.println("Manufacturer: "+i.manufacturerName+
					" Model: "+i.carModel+"Price :"+
					i.carPrice);
		System.out.println();
	}
}
