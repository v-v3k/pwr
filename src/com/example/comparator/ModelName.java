package com.example.comparator;

import java.util.Comparator;

public class ModelName implements Comparator<Cars> {
	public int compare(Cars obj1, Cars obj2)
	{
		Cars car1=obj1;
		Cars car2=obj2;
		return car1.carModel.compareTo(car2.carModel);
	}
}
