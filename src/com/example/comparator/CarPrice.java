package com.example.comparator;

import java.util.Comparator;
public class CarPrice implements Comparator<Cars> {
	public int compare(Cars obj1, Cars obj2) {
		if(obj1.carPrice>obj2.carPrice)
			return 1;
		else if(obj1.carPrice<obj2.carPrice)
			return -1;
		else
			return 0;
	}

}
