package com.example.comparable;

class ComparableExample implements Comparable<ComparableExample> {
	public int indexCounter;
	public ComparableExample(int indexCounter) {
		this.indexCounter=indexCounter;
	}
	public int compareTo(ComparableExample obj1) {
		if(indexCounter<obj1.indexCounter)
			return 1;
		else if(indexCounter>obj1.indexCounter)
			return -1;
		return 0;
	}
	
}
