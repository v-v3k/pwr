package com.example.comparable;

import java.util.ArrayList;
import java.util.Collections;

public class PgmRoot {

	public static void main(String[] args) {
		ArrayList<ComparableExample> al=new ArrayList<ComparableExample>();
		al.add(new ComparableExample(150));
		al.add(new ComparableExample(120));
		al.add(new ComparableExample(150));
		al.add(new ComparableExample(140));
		al.add(new ComparableExample(170));
		al.add(new ComparableExample(100));
		al.add(new ComparableExample(180));
		Collections.sort(al);
		for(ComparableExample i:al)
			System.out.println(i.indexCounter);
	}

}
