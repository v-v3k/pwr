package com.example.inheritance;

class h1
{
	public int displayResult(int val)
	{
		return val;
	}
}

class aoc extends h1
{
	int r;
	public void setArea(int r)
	{
		this.r=(int)(3.14*(r*r));
	}
}

class aos extends h1
{
	int s;
	public void setArea(int s)
	{
		this.s=(s*s);
	}
}