package com.example.inheritance;

class a
{
	public void x1()
	{
		System.out.println("Parent 1...");
	}
}

class b extends a
{
	public void y1()
	{
		System.out.println("A's child and C's parent...");
	}
}

class c extends b
{
	public void z1()
	{
		System.out.println("Child class of B...");
	}
}