package com.example.inheritance;


class x
{
	public void x1()
	{
		System.out.println("Parent class method...");
	}
}

class y extends x
{
	public void y1()
	{
		System.out.println("Child class method...");
	}
}