package com.example.inheritance;

interface root
{
	abstract int calculate();
	abstract void setVal(int rad);
}

class r_or_s
{
	int rs;
	public void setRS(int rs)
	{
		this.rs = rs;
	}
}

class circle extends r_or_s implements root
{
	public void setVal(int rad)
	{
		super.setRS(rad);
	}
	public int calculate()
	{
		return (int)(3.14*(super.rs*super.rs));
	}
}

class square extends r_or_s implements root
{
	public void setVal(int side)
	{
		super.setRS(side);
	}
	public int calculate()
	{
		return (super.rs*super.rs);
	}
}
