package com.example.inheritance;

import java.util.Scanner;

public class PgmRoot {
	
	public static Scanner sc=new Scanner(System.in);
	public static void Single()
	{
		y yobj=new y();
    	yobj.x1();
    	yobj.y1();
	}
	
	public static void Multiple()
	{
		int n=0;
		root r;
		while(n!=-1)
		{
			System.out.print("Which area to find?\n1. Circle\n2. Square\nEnter Choice: ");
			n=sc.nextInt();
			if(n==1)
				r=new circle();
			else if(n==2)
				r=new square();
			else
			{
				n=-1;
				continue;
			}
			System.out.print("Enter value: ");
			n=sc.nextInt();
			r.setVal(n);
			System.out.println("Area: "+r.calculate());
			n=-1;
		}
	}
	
	public static void Hierarchical()
	{
		System.out.print("Enter value: ");
		int val=sc.nextInt();
		// Area of circle
		aoc c=new aoc();
		c.setArea(val);
		System.out.println("Area of circle: "+c.displayResult(c.r));
		
		// Area of square
		aos s=new aos();
		s.setArea(val);
		System.out.println("Area of circle: "+s.displayResult(s.s));
	}
	
	public static void MultiLevel()
	{
		c cobj=new c();
    	cobj.x1();
    	cobj.y1();
    	cobj.z1();
	}
	
	public static int perfTask(int choice)
	{
		switch(choice)
		{
			case 1:
				Single();
				break;
			case 2:
				Multiple();
				break;
			case 3:
				MultiLevel();
				break;
			case 4:
				Hierarchical();
				break;
			default:
				return -1;
		}
		return 0;
	}

	public static void main(String[] args) {
		// Root for all programs in this package
		int choice=0;
		
		while(choice!=-1)
		{
			System.out.println("1. Single");
			System.out.println("2. Multiple");
			System.out.println("3. Multilevel");
			System.out.println("4. Hierarchical");
			System.out.println("Any other value will exit the program...");
			System.out.print("Choice: ");
			choice=sc.nextInt();
			if(perfTask(choice)==-1)
				choice=-1;
		}
	}

}
