package com.example.customException;

import java.util.Scanner;

class phNumValidationFailure extends Exception
{
	phNumValidationFailure(String msg)
	{
		super(msg);
	}
}

public class CustExcept {

	public static int isNumeric(String value) throws NumberFormatException
	{
		try
		{
			int val=Integer.parseInt(value);
			return 0;
		}
		catch(NumberFormatException ex)
		{
			throw new NumberFormatException();
		}
	}
	
	public static void main(String[] args) throws phNumValidationFailure {
		String phNum;
		System.out.print("Enter your phone number: ");
		Scanner sc=new Scanner(System.in);
		phNum=sc.nextLine();
		if(phNum.length()!=10 || isNumeric(phNum)!=0)
		{
			throw new phNumValidationFailure("Invlaid phone number passed!");
		}
		else
		{
			System.out.println("Valid number supplied!");
		}
		sc.close();
	}

}
