package com.example.collections;

import java.util.*;

public class ArrayListExample {

	public void arraylistOpr() {
		ArrayList<Integer> numList=new ArrayList<Integer>();
		
		// Add items
		numList.add(11);
		numList.add(30);
		numList.add(10);
		numList.add(20);
		numList.add(40);
		numList.add(60);
		numList.add(40);
		numList.add(50);
		System.out.println("Size: " + numList.size() + "\nData: " + numList);

		// List individual items
		System.out.println("Individual Items: ");
		for(int i : numList)
			System.out.println(i);
		
		// Remove individual items
		numList.remove(7);
		System.out.println("Size: " + numList.size() + "\nData: " + numList);
		
		// Sort using Collections
		Collections.sort(numList);
		System.out.println("Size: " + numList.size() + "\nData: " + numList);
		
		// Clear all items
		numList.clear();
		System.out.println("Size: " + numList.size() + "\nData: " + numList);
	}

}
