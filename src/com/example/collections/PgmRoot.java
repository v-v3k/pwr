package com.example.collections;

import java.util.Scanner;

public class PgmRoot {
	
	public static int perfTask(int choice)
	{
		switch(choice)
		{
			case 1:
				VectorExample vex=new VectorExample();
				vex.vectorOpr();
				break;
			case 2:
				ArrayListExample alex=new ArrayListExample();
				alex.arraylistOpr();
				break;
			case 4:
				LinkedListExample llex=new LinkedListExample();
				llex.linkedlistOpr();
				break;
			case 3:
				HashMapExample hmex=new HashMapExample();
				hmex.hashmapOpr();
				break;
			case 5:
				TreeMapExample tmex=new TreeMapExample();
				tmex.treemapOpr();
				break;
			default:
				return -1;
		}
		return 0;
	}

	public static void main(String[] args) {
		// Root for all programs in this package
		int choice=0;
		Scanner sc=new Scanner(System.in);
		while(choice!=-1)
		{
			System.out.println("1. Vector Example");
			System.out.println("2. ArrayList Example");
			System.out.println("3. HashMap Example");
			System.out.println("4. LinkedList Example");
			System.out.println("Any other value will exit the program...");
			System.out.print("Choice: ");
			if(perfTask(sc.nextInt())==-1)
				choice=-1;
		}
		sc.close();
	}

}
