package com.example.collections;

import java.util.*;

public class VectorExample {
	public void vectorOpr() {
		// Create vector of size 5 and auto increment of size 1
		Vector vec=new Vector(3,1);
		
		// Add item to vector
		vec.add(1);
		vec.add("PI");
		vec.add(3.14);
		System.out.println("Size: "+vec.size()+" Values: "+vec);
		
		// Add item at specific position
		vec.add(2,"Added");
		System.out.println("Size: "+vec.size()+" Values: "+vec);
		
		// Add collections object
		ArrayList<String> al=new ArrayList<String>(Arrays.asList("val001","val002","val003","val004","val005"));
		vec.addAll(al);
		System.out.println("Size: "+vec.size()+" Values: "+vec);
		
		// Clear vector
		vec.clear();
		System.out.println("Size: "+vec.size()+" Values: "+vec);
	}
}
