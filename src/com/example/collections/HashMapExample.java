package com.example.collections;

import java.util.*;

public class HashMapExample {
	public void hashmapOpr() {
		HashMap<String, String> hm=new HashMap<String, String>();
		// Add values
		hm.put("Name", "Abcd");
		hm.put("Age", "25");
		hm.put("Gender", "M");
		hm.put("Phone", "1234567890");
		System.out.println(hm);
		
		// Get specific value
		System.out.println("Age: "+hm.get("Age"));
		
		// Removing a specific value using key
		hm.remove("Phone");
		System.out.println(hm);
		
		// Size
		System.out.println("Size: " + hm.size());
		
		// Print Key Set
		System.out.println("Keys: " + hm.keySet());
		
		// Print Value Set
		System.out.println("Values: " + hm.values());
		
		// Clearing all values
		hm.clear();
		System.out.println(hm);
	}

}
