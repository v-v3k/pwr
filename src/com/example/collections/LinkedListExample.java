package com.example.collections;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListExample {

	@SuppressWarnings("unchecked")
	public void linkedlistOpr() {
		LinkedList<String> ll=new LinkedList<String>(Arrays.asList("LinkedListItem001","LinkedListItem002"));
		
		// Add element to LinkedList
		ll.add(1,"LinkedListItem001.5");
		System.out.println("Size: "+ll.size()+" Items: "+ll);
		
		// Adding value to the beginning
		ll.addFirst("LinkedListItem000");
		System.out.println("Size: "+ll.size()+" Items: "+ll);
		
		// Adding value to the last
		ll.addLast("LinkedListItem003");
		System.out.println("Size: "+ll.size()+" Items: "+ll);
		
		// Adding value to the last
		System.out.println("Check if LinkedListItem001.5 is present in the list: "+ll.contains("LinkedListItem001.5"));
		
		// Copy LinkedList items to another list
		LinkedList<String> ll1=new LinkedList<String>();
		ll1=(LinkedList<String>) ll.clone();
		System.out.println("Size: "+ll1.size()+" Cloned Items: "+ll1);
		
		// Using iterator to display value of LinkedList
		Iterator<String> iter=ll.iterator();
		int i=1;
		while(iter.hasNext())
			System.out.println("Item "+(i++)+": "+iter.next());
		
		// Using descending iterator to display value of LinkedList
		Iterator<String> reviter=ll.descendingIterator();
		i=1;
		while(reviter.hasNext())
			System.out.println("Item "+(i++)+": "+reviter.next());
		
		// Cleared clone LinkedList
		ll1.clear();
		System.out.println("Size: "+ll1.size()+" Cloned Items: "+ll1);
		
		// Push and pop operations
		ll1.push("Hello");
		System.out.println("Size: "+ll1.size()+" Items: "+ll1);
		ll1.pop();
		System.out.println("Size: "+ll1.size()+" Items: "+ll1);
	}

}
